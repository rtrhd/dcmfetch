# Simple DICOM Server Access from Python #

## Introduction ##

The `dcmfetch` package provides a python layer around command line tools for access to [DICOM](http://medical.nema.org/) servers. It currently supports the [dcm4che3](https://sourceforge.net/projects/dcm4che/files/dcm4che3/) toolkit. It also wraps the native python DICOM transport library [pynetdicom](https://github.com/pydicom/pynetdicom) and [requests]() based access to the dicom `QIDO-RS` and `WADO-RS` web interfaces with the same siimplified interface.

As it stands a DICOM server must support either the DICOM services`c-find` and `c-get` or the newer [REST](https://en.wikipedia.org/wiki/Representational_state_transfer) apis `QIDO-RS` and `WADO-RS`.

The package provides a top level python routine `fetch_series` which returns a series of [pydicom](http://www.pydicom.org/) objects, a [PyQt](https://riverbankcomputing.com/software/pyqt/intro) dialog, a command line script `dcmfetch` and a gui tool `dcmfetchtool` that uses the dialog.

DICOM servers are identified indirectly by keys in the configuration file `dcmnodes.cf`. This file encodes the server details (AET, most, port) together with a string representing facilities supported by the server (including the web API if available). The file is typically installed to the directory `/etc` on a unix system, but a per user version may be installed in `~/.config/dcmfetch`.

## Dependencies ##
The package is compatible with python 2.7 and python 3.5 or later. Reasonably recent versions of the [requests](http://docs.python-requests.org/en/master/) package is required along with [qtpy](https://github.com/spyder-ide/qtpy) - an abstraction layer over different versions of PyQt. Any of PyQt5, PyQt4 or PySide are supported. For the (optional) [pynetdicom](https://github.com/pydicom/pynetdicom) a recent version of this package is required.

In the absence of a suitable version of [pynetdicom](https://github.com/pydicom/pynetdicom) the package relies on dcm4che3 command line tools for the communication with the server using the DICOM protocol. Standalone versions of the required tools for both posix and mswindows systems are included in a package subdirectory so do not require separate installation. A java runtime is still needed though as this is not bundled with the tools. Currently, the package defaults to trying the `dcm4che3` tools first. This can be overridden by specifying `DCMTOOLKIT=pynetdicom` in the environment. 

## Installation ##

The package is [pip](https://docs.python.org/3/installing/) installable from the following location:

```
git+https://rtrhd@bitbucket.org/rtrhd/dcmfetch.git
```

Alternatively, the repository may be cloned and the package installed with `python setup.py install`.

## Configuration ##

DICOM nodes are identified by keys in the configuration file `dcmnodes.cf`. This should be installed in a discoverable location. On POSIX systems this will be `/etc/dcmnodes.cf` for system wide configuration or `~/.config/dcmfetch/dcmnodes.cf` for per user versions. On an ms-windows system the `%APPDATA%` and `%LOCALAPPDATA%` are searched. The file has one line for each server and encodes the server details (entity title, node, port) together with a string representing facilities supported by the server (including the web API if available).

There is an example nodes file in the repository root and a fall back one as part of the package.

The lines are of the format:

```
ServerName AET host port facilities [auth]
```

with whitespace field separation and comment allowed with an initial `#`. No whitespace is allowed in any of the fields. The host may be specified by domain name or ip address. The facilities string is a sequence of
characters with the following meanings:

|Code|Meaning                |
|----|-----------------------|
|F   |Supports C-FIND        |
|G   |Supports C-GET         |
|W   |Supports REST interface|

In the case of the REST interface the `aet` field is used for the REST 'endpoint' and the optional final field is used to specify a `username:password` field for authentication if required. Note that this is not a secure solution as this file will be generally readable. Other characters `SMXIQCABL` are supported for historical reasons but are not useful here and should be avoided.

## Python Interface ##

The package provides simple functions for downloading one or more series (`dcmfetch.dcmftech`) as well as a lower level query interface (`dcmfetch.queryinterface`). There is also a qt-based dialog that allows browsing of a DICOM store so a series can be selected for
downloading (`dcmfetch.fetchdialog`).
  
## Tools ##

There are two programs provided with the package:

### dcmfetch ###

This is a command line tool for fetching a list of series from a specified server.
It has the following synopsis:

```
usage: dcmfetch [-h] [-a ARCHIVE] -p PATID [-S STUDY] -s SERIES [-z] [-o OUT]
                [-V]

Fetch DICOM series from Archive Server

optional arguments:
  -h, --help            show this help message and exit
  -a ARCHIVE, --archive ARCHIVE
                        Name of archive server in dicom nodes file
  -p PATID, --patid PATID
                        Patient to retrieve scans for (an exact string only)
  -S STUDY, --study STUDY
                        Study to retrieve scans for (may be a glob pattern)
  -s SERIES, --series SERIES
                        Series number, list or range; can specify multiple
                        times
  -z, --zip             Pack dicom objects into zip file
  -o OUT, --out OUT     Output directory to store series in
  -V, --version         show program's version number and exit

Specify series numbers as a comma separated list of integers and ranges
without spaces e.g. "-s 1-5,7,8,10-12".
```

### dcmfetchtool ###

This is a graphical widget that opens a DICOM `dialog` for selecting a (currently single) series to download.
The downloaded series may then be saved as a zip archive.

## Limitations and Notes ##

For DICOM native communications the caller also needs to be identified with an AET.
This is set automatically based on the hostname and is of the form `<hostname>Query`. This may cause issues
in the configuration of some DICOM servers that validate calling AETs.

There is currently no support for sending images to a DICOM server, though this is possible independently with the `dcm4che3` command line tools, `pynetdicom` and the DICOM REST API.

Only a single Series may be selected for download in the DICOM dialog.